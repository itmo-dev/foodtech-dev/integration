import './styles.css';
var ref;
var check;
export default class PosterITMO extends React.Component {
    constructor(props) {
        super(props);
        // Показываем кнопки приложения в окне настроек и заказа
        Poster.interface.showApplicationIconAt({
            order: 'Печать чека с QR-кодом',
            payment: 'Печать чека с QR-кодом'
        });

        // Подписываемся на клик по кнопке
        Poster.on('applicationIconClicked', () => {
            Poster.orders.getActive()
                .then(function (order) {
                    console.log('active order', order);
                    Poster.makeApiRequest('settings.getAllSettings', {
                        method: 'get'
                    }, (settings) => {
                        if (settings) {
                            ref = 'https://foodtechmoneymaker.herokuapp.com/integration/' + settings.company_name + '/';
                            ref = encodeURI(ref);
                            Poster.makeRequest(ref, {
                                method: 'get',
                                headers: [
                                    'Content-Type: application/json'
                                ]
                            }, (answer) => {
                                if (answer && Number(answer.code) === 200) {
                                    var answ = JSON.parse(answer.result);
                                    var mycheck = order.order.orderName;
                                    check = answ.url + '?cid=' + mycheck;
                                    var myid = order.order.id;
                                    Poster.orders.printReceipt(myid, check, 1234);
                                }
                            });
                        }
                    });
                })
        });
    }
}
